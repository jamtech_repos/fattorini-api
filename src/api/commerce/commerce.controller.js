import express from "express";
import CommerceProvider from "./commerce.provider";
var router = express.Router();

router.post("/commerce",
  /* Guardar registro */
  async (req, res) => {
    try {
      let data = await CommerceProvider.Save(req.body)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(error.code || 500).send(error)
    }
  });

router.put('/commerce',
  /* Actualiza la data de un usuario */
  async (req, res) => {
    try {
      let id = req.query.id
      let body = req.body
      let data = await CommerceProvider.Update(id, body)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)

router.delete('/commerce',
  /* Eliminar un usuario */
  async (req, res) => {
    try {
      let id = req.query.id
      let data = await CommerceProvider.Delete(id)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)


router.get('/commerce/list',
  /* Listar todos los desarrolladores */
  async (req, res) => {
    try {
      let data = await CommerceProvider.ListAll()
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
router.get('/commerce',
  /* Listar todos los desarrolladores */
  async (req, res) => {
    try {
      let data = await CommerceProvider.GetCommerce(req.query.id)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
export default router;