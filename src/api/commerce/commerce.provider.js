'use strict';

import CommerceModel from './commerce.model';
import AdminsModel from '../adminscommerce/admins.model';

export default {

  async Save(body) {
    try {
      let data = new CommerceModel(body);
      let admins = new AdminsModel({
        commerce: data._id
      }).save()
      return data.save()
    } catch (error) {
      throw error;
    }
  },

  async Update(id, body) {
    try {
      return await CommerceModel.findByIdAndUpdate(id, body).exec();
    } catch (error) {
      throw error;
    }
  },

  async Delete(id) {
    try {
      await AdminsModel.findOneAndDelete({
        commerce: id
      })
      return await CommerceModel.findByIdAndDelete(id).exec();

    } catch (error) {
      throw error;
    }
  },

  async ListAll() {
    try {
      return await CommerceModel.find().exec();
    } catch (error) {
      throw error;
    }
  },
  async GetCommerce(id) {
    try {
      return await CommerceModel.findById(id).exec();
    } catch (error) {
      throw error;
    }
  }
};