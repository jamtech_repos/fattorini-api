'use strict';

import {
  Schema,
  model
} from 'mongoose';

const CommerceModel = Schema({
  name: String,
  rif: String,
  description: String,
  address: String,
  city: String,
  state: String,
  picture: String,
  category: [{
    type: Schema.Types.ObjectId,
    ref: 'Category',
  }, ],
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
});

export default model('Commerce', CommerceModel);