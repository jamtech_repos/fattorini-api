'use strict';

import UserModel from './user.model';
import jwt from '../../config/jwt';
import bcrypt from 'bcryptjs';
import AdminProvider from '../adminscommerce/admins.provider';

export default {
    async Register(body) {
        try {
            let findUser = await UserModel.findOne({
                email: body.email,
            }).exec();
            if (!findUser) {
                body.password = bcrypt.hashSync(body.password, 10);
                let user = new UserModel(body);
                return user.save();
            } else
                throw {
                    code: 403,
                    error: 'Email already used',
                };
        } catch (error) {
            throw error;
        }
    },
    async RegisterCA(body, commerce) {
        try {
            let findUser = await UserModel.findOne({
                email: body.email,
            }).exec();
            if (!findUser) {
                body.password = bcrypt.hashSync(body.password, 10);
                body.role = 'CA';
                let user = new UserModel(body)
                user.save();
                let admins = await AdminProvider.Update(commerce, user);
                console.log('register->', admins)
                return {
                    user: user,
                    token: jwt.createToken(user),
                    role: user.role,
                };
            } else
                throw {
                    code: 403,
                    error: 'Email already used',
                };
        } catch (error) {
            throw error;
        }
    },

    async Login(email, password) {
        try {
            const user = await UserModel.findOne({
                email,
            }).exec();
            if (user) {
                if (!bcrypt.compareSync(password, user.password))
                    throw {
                        code: 400,
                        error: 'password invalid',
                    };
                else
                    return {
                        token: jwt.createToken(user),
                        role: user.role,
                    };
            } else
                throw {
                    code: 404,
                    error: 'email invalid',
                };
        } catch (error) {
            throw error;
        }
    },
    async UserExist(email) {
        try {
            let findUser = await UserModel.findOne({
                email: email,
            }).exec();
            if (!findUser) {
                return {
                    exist: false
                };
            } else
                throw {
                    code: 403,
                    error: 'Ya existe un usuario con este correo',
                };
        } catch (error) {
            throw error;
        }
    },
    async GetUserLogin(id) {
        try {
            return UserModel.findById(id, {
                password: 0
            }).exec();
        } catch (error) {
            throw error;
        }
    },
    async Update(id, body) {
        try {
            return UserModel.findByIdAndUpdate(id, body).exec();
        } catch (error) {
            throw error;
        }
    },

    async Delete(id) {
        try {
            return UserModel.findByIdAndDelete(id).exec();
        } catch (error) {
            throw error;
        }
    },
    async ListByRole(role) {
        try {
            return UserModel.find({
                role: role
            }, {
                password: 0
            }).exec();
        } catch (error) {
            throw error;
        }
    },
};