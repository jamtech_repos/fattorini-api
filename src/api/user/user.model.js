'use strict';

import {
    Schema,
    model
} from 'mongoose';

const UserModel = Schema({
    email: {
        type: String,
    },
    password: {
        type: String,
    },
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
    status: {
        type: Boolean,
        default: true
    },
    phone: {
        type: String,
        default: 'Desconocido'
    },
    role: {
        type: String,
        default: 'C',
        enum: ['A', 'CA', 'C', 'D']
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default model('User', UserModel);