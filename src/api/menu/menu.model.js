'use strict';

import {
  Schema,
  model
} from 'mongoose';

const MenuModel = Schema({
  name: {
    type: String,
  },
  depth: Number,
  id: Number,
  commerce: {
    type: Schema.Types.ObjectId,
    ref: 'Commerce'
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

export default model('Menu', MenuModel);