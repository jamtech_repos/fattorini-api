'use strict';

import MenuModel from './menu.model';

export default {
  async Save(body) {
    try {
      let data = new MenuModel(body);
      return data.save();
    } catch (error) {
      throw error;
    }
  },

  async Update(id, body) {
    try {
      return await MenuModel.findByIdAndUpdate(id, body).exec();
    } catch (error) {
      throw error;
    }
  },
  async UpdateAll(array) {
    try {
      for (let i = 0; i < array.length; i++) {
        const element = array[i];
        element.id = i
        await MenuModel.findByIdAndUpdate(element._id, element).exec();
      }
      return await MenuModel.find().exec();

    } catch (error) {
      throw error;
    }
  },

  async Delete(id) {
    try {
      return await MenuModel.findByIdAndDelete(id).exec();
    } catch (error) {
      throw error;
    }
  },

  async ListAll(commerce) {
    try {
      return await MenuModel.find({commerce:commerce}).sort({
        id: 1
      }).exec();
    } catch (error) {
      throw error;
    }
  },
  async ListAllAWithChildren() {
    try {
      var _categories = [];
      let categories = await MenuModel.find().exec();
    } catch (error) {
      throw error;
    }
  },
};

const orderList = (array, lastitem) => {
  if (!array[0] || !array) {
    return;
  } else {
    if (array[0].depth === lastitem.depth) {
      return array[0].concat(orderList(array.split(1), array[0]));
    } else if (array[0].depth > lastitem.depth) {
      lastitem.children.push(array[0]);
      return orderList(array.split(1), array[0]);
    } else if (array[0].depth < lastitem.depth) {
      return orderList(array.split(1), array[0]);
    }
  }
};