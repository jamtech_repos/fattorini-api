import express from "express";
import MenuProvider from "./menu.provider";
var router = express.Router();

router.post("/menu",
  /* Guardar registro */
  async (req, res) => {
    try {
      let data = await MenuProvider.Save(req.body)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(error.code || 500).send(error)
    }
  });

router.put('/menu',
  /* Actualiza la data de un usuario */
  async (req, res) => {
    try {
      let id = req.query.id
      let body = req.body
      let data = await MenuProvider.Update(id, body)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
router.put('/menu/all',
  /* Actualiza la data de un usuario */
  async (req, res) => {
    try {
      let body = req.body.array
      let data = await MenuProvider.UpdateAll(body)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)

router.delete('/menu',
  /* Eliminar un usuario */
  async (req, res) => {
    try {
      let id = req.query.id
      let data = await MenuProvider.Delete(id)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)


router.get('/menu/list',
  /* Listar todos los desarrolladores */
  async (req, res) => {
    try {
      let data = await MenuProvider.ListAll(req.query.commerce)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
export default router;