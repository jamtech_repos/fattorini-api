'use strict';

import {
  Schema,
  model
} from 'mongoose';

const AppConfigModel = Schema({
  exchange: Number,
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

export default model('AppConfig', AppConfigModel);