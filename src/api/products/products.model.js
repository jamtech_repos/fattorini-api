'use strict';

import {
  Schema,
  model
} from 'mongoose';

const ProductModel = Schema({
  name: {
    type: String,
  },
  description: {
    type: String,
  },
  price: Number,
  currency: {
    type: String,
    dafault: 'USD'
  },
  picture: [{
    type: String
  }],
  offer: {
    type: Boolean,
    default: false
  },
  offerpercent: {
    type: Number,
    default: 0
  },
  status: {
    type: Boolean,
    default: true,
  },
  stock_status: {
    type: Boolean,
    default: true,
  },
  menu: {
    type: Schema.Types.ObjectId,
    ref: 'Menu',
  },
  commerce: {
    type: Schema.Types.ObjectId,
    ref: 'Commerce',
  },
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
});

export default model('Product', ProductModel);