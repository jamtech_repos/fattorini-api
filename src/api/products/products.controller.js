import express from "express";
import ProductProvider from "./products.provider";
var router = express.Router();

router.post("/product",
  /* Guardar registro */
  async (req, res) => {
    try {
      let product = await ProductProvider.Save(req.body)
      res.status(200).send(product)
    } catch (error) {
      console.log(error)
      res.status(error.code || 500).send(error)
    }
  });

router.put('/product',
  /* Actualiza la data de un usuario */
  async (req, res) => {
    try {
      let id = req.query.id
      let body = req.body
      let data = await ProductProvider.Update(id, body)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
router.delete('/product',
  /* Eliminar un usuario */
  async (req, res) => {
    try {
      let id = req.query.id
      let data = await ProductProvider.ForceDelete(id)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
router.put('/product/change_status',
  /* Eliminar un usuario */
  async (req, res) => {
    try {
      let id = req.query.id
      let status = req.body.status
      let data = await ProductProvider.Delete(id, status)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
router.get('/product/commerce',
  /* Eliminar un usuario */
  async (req, res) => {
    try {
      let commerce = req.query.commerce
      let data = await ProductProvider.GetByCommerce(commerce)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
router.get('/product/menu',
  /* Eliminar un usuario */
  async (req, res) => {
    try {
      let menu = req.query.menu
      let data = await ProductProvider.GetByMenu(menu)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
router.get('/product/menu/count',
  /* Eliminar un usuario */
  async (req, res) => {
    try {
      let menu = req.query.menu
      let data = await ProductProvider.GetByMenu(menu)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)


export default router;