'use strict';

import ProductModel from './products.model';

export default {

  async Save(body) {
    try {
      let products = new ProductModel(body);
      return products.save()
    } catch (error) {
      throw error;
    }
  },

  async Update(id, body) {
    try {
      return await ProductModel.findByIdAndUpdate(id, body).exec();
    } catch (error) {
      throw error;
    }
  },
  async GetByCommerce(commerce) {
    try {
      return await ProductModel.find({
        commerce: commerce
      }).exec();
    } catch (error) {
      throw error;
    }
  },
  async GetByMenu(Menu) {
    try {
      return await ProductModel.find({
        menu: menu
      }).exec();
    } catch (error) {
      throw error;
    }
  },
  async GetCountByMenu(Menu) {
    try {
      return await ProductModel.count({
        menu: menu
      }).exec();
    } catch (error) {
      throw error;
    }
  },
  async Delete(id, status) {
    try {
      return await ProductModel.findByIdAndUpdate(id, {
        status: status
      }).exec();
    } catch (error) {
      throw error;
    }
  },
  async ForceDelete(id, status) {
    try {
      return await ProductModel.findByIdAndDelete(id).exec();
    } catch (error) {
      throw error;
    }
  },
};