'use strict';

import CommentsModel from './comments.model';

export default {
  async Save(body) {
    try {
      let category = new CommentsModel(body);
      return category.save();
    } catch (error) {
      throw error;
    }
  },

  async Update(id, body) {
    try {
      return await CommentsModel.findByIdAndUpdate(id, body).exec();
    } catch (error) {
      throw error;
    }
  },

  async Delete(id) {
    try {
      return await CommentsModel.findByIdAndDelete(id).exec();
    } catch (error) {
      throw error;
    }
  },

  async ListAll() {
    try {
      return await CommentsModel.find().sort({
        createdAt: 1
      }).exec();
    } catch (error) {
      throw error;
    }
  },
  async ListByCommerce(commerce) {
    try {
      return await CommentsModel.find({
        commerce: commerce
      }).sort({
        createdAt: 1
      }).exec();
    } catch (error) {
      throw error;
    }
  },
};