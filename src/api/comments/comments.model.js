'use strict';

import {
  Schema,
  model
} from 'mongoose';

const CommentsModel = Schema({
  text: String,
  score: Number,
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  commerce: {
    type: Schema.Types.ObjectId,
    ref: 'Commerce'
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

export default model('Comments', CommentsModel);