import express from "express";
import CommentsProvider from "./comments.provider";
var router = express.Router();

router.post("/comment",
  /* Guardar registro */
  async (req, res) => {
    try {
      let user = await CommentsProvider.Save(req.body)
      res.status(200).send(user)
    } catch (error) {
      console.log(error)
      res.status(error.code || 500).send(error)
    }
  });

router.put('/comment',
  /* Actualiza la data de un usuario */
  async (req, res) => {
    try {
      let id = req.query.id
      let body = req.body
      let data = await CommentsProvider.Update(id, body)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)

router.delete('/comment',
  /* Eliminar un usuario */
  async (req, res) => {
    try {
      let id = req.query.id
      let data = await CommentsProvider.Delete(id)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)


router.get('/comment/list',
  /* Listar todos los desarrolladores */
  async (req, res) => {
    try {
      let data = await CommentsProvider.ListAll()
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
router.get('/comment/list/commerce',
  /* Listar todos los desarrolladores */
  async (req, res) => {
    try {
      let commerce = req.query.commerce
      let data = await CommentsProvider.ListByCommerce(commerce)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
export default router;