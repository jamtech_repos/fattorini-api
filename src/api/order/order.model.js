'use strict';

import {
  Schema,
  model
} from 'mongoose';

const OrderModel = Schema({
  status: {
    type: String,
    default: '1'
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  product: {
    type: Schema.Types.ObjectId,
    ref: 'Product'
  },
  commerce: {
    type: Schema.Types.ObjectId,
    ref: 'Commerce'
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

export default model('Order', OrderModel);