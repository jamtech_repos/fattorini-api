'use strict';

import {
  Schema,
  model
} from 'mongoose';

const CategoryModel = Schema({
  name: {
    type: String,
  },
  depth: Number,
  id: Number
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

export default model('Category', CategoryModel);