'use strict';

import CategoryModel from './category.model';

export default {
  async Save(body) {
    try {
      let category = new CategoryModel(body);
      return category.save();
    } catch (error) {
      throw error;
    }
  },

  async Update(id, body) {
    try {
      return await CategoryModel.findByIdAndUpdate(id, body).exec();
    } catch (error) {
      throw error;
    }
  },
  async UpdateAll(array) {
    try {
      for (let i = 0; i < array.length; i++) {
        const element = array[i];
        element.id = i
        await CategoryModel.findByIdAndUpdate(element._id, element).exec();
      }
      return await CategoryModel.find().exec();

    } catch (error) {
      throw error;
    }
  },

  async Delete(id) {
    try {
      return await CategoryModel.findByIdAndDelete(id).exec();
    } catch (error) {
      throw error;
    }
  },

  async ListAll() {
    try {
      return await CategoryModel.find().sort({
        id: 1
      }).exec();
    } catch (error) {
      throw error;
    }
  },
  async ListAllAWithChildren() {
    try {
      var _categories = [];
      let categories = await CategoryModel.find().exec();
    } catch (error) {
      throw error;
    }
  },
};

const orderList = (array, lastitem) => {
  if (!array[0] || !array) {
    return;
  } else {
    if (array[0].depth === lastitem.depth) {
      return array[0].concat(orderList(array.split(1), array[0]));
    } else if (array[0].depth > lastitem.depth) {
      lastitem.children.push(array[0]);
      return orderList(array.split(1), array[0]);
    } else if (array[0].depth < lastitem.depth) {
      return orderList(array.split(1), array[0]);
    }
  }
};