import express from "express";
import AdminsProvider from "./admins.provider";
var router = express.Router();

router.get('/admins/commerce',
  /* Listar todos los desarrolladores */
  async (req, res) => {
    try {
      let data = await AdminsProvider.GetCommerceByAdmin(req.query.user)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
router.get('/admins/list',
  /* Listar todos los desarrolladores */
  async (req, res) => {
    try {
      let data = await AdminsProvider.ListAdmins(req.query.commerce)
      res.status(200).send(data)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
)
export default router;