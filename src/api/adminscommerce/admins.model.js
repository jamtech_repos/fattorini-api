'use strict';

import {
  Schema,
  model
} from 'mongoose';

const AdminCommerceModel = Schema({
  user: [{
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  }],
  commerce: {
    type: Schema.Types.ObjectId,
    ref: 'Commerce'
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

export default model('AdminCommerce', AdminCommerceModel);