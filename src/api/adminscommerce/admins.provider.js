'use strict';

import AdminsModel from './admins.model';

export default {
  async Update(id, user) {
    try {
      let data = await AdminsModel.findOne({
        commerce: id,
      }).exec();
      data.user.push(user);
      console.log(data, user);
      return await AdminsModel.findByIdAndUpdate(data._id, data).exec();
    } catch (error) {
      throw error;
    }
  },

  async Delete(id) {
    try {
      return AdminsModel.findByIdAndDelete(id).exec();
    } catch (error) {
      throw error;
    }
  },

  async ListAdmins(commerce) {
    try {
      return AdminsModel.findOne({
        commerce: commerce,
      })
        .populate('user')
        .exec();
    } catch (error) {
      throw error;
    }
  },
  async GetCommerceByAdmin(user) {
    try {
      return AdminsModel.findOne(
        {
          user: {
            $in: [user],
          },
        },
        {
          user: 0,
        }
      )
        .populate('commerce')
        .exec();
    } catch (error) {
      throw error;
    }
  },
};
