import app from '../app';
import {
  createServer
} from 'http';

var server = createServer(app);

import {
  connect
} from 'mongoose';
const PORT = process.env.PORT || 8000;
connect(
  'mongodb://fattorini:fattorini2020@ds153400.mlab.com:53400/heroku_6vz2m1sv', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  },
  (err, res) => {
    if (err) {
      throw err;
    } else {
      server.listen(PORT);
      console.log('Base de datos conectada!');
      console.log('Servidor escuchando por el puerto ' + PORT);
    }
  }
);