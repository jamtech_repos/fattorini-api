import express from "express";
import UserController from '../api/user/user.controller'
import CategoryController from '../api/category/category.controller'
import ProductController from '../api/products/products.controller'
import OrderController from '../api/order/order.controller'
import CommerceController from '../api/commerce/commerce.controller'
import MenuController from '../api/menu/menu.controller'
import CommentsController from '../api/comments/comments.controller'
import AdminsController from "../api/adminscommerce/admins.controller";

var router = express.Router();

router.use('/api', UserController)
router.use('/api', CategoryController)
router.use('/api', ProductController)
router.use('/api', OrderController)
router.use('/api', CommerceController)
router.use('/api', MenuController)
router.use('/api', CommentsController)
router.use('/api', AdminsController)

export default router;